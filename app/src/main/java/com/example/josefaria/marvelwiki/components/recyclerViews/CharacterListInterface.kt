package com.example.josefaria.marvelwiki.components.recyclerViews

import com.example.josefaria.marvelwiki.models.room.Favourite

interface CharacterListInterface {

    fun openCharacter(id: Long)

    fun favouritePressed(favourite: Favourite)

}