package com.example.josefaria.marvelwiki.models.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [Favourite::class], version = 1)
abstract class FavouriteDatabase : RoomDatabase() {
    abstract fun favouriteDao(): FavouriteDao

    companion object {

        var TEST_MODE = false

        @Volatile
        private var INSTANCE: FavouriteDatabase? = null

        fun getDatabase(context: Context): FavouriteDatabase {
            return INSTANCE ?: synchronized(this) {

                val instance =  if(!TEST_MODE) {
                    // Create database here
                     Room.databaseBuilder(
                        context.applicationContext,
                        FavouriteDatabase::class.java,
                        "favourite_database"
                    ).build()
                }
                else
                {
                    Room.inMemoryDatabaseBuilder(
                        context.applicationContext,
                        FavouriteDatabase::class.java).allowMainThreadQueries().build()
                }

                INSTANCE = instance
                instance
            }
        }
    }

}