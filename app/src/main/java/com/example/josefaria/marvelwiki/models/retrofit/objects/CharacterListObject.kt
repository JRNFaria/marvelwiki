package com.example.josefaria.marvelwiki.models.retrofit.objects

import com.google.gson.annotations.SerializedName

class CharacterListObject {

    @SerializedName("code")
    var code = -1

    @SerializedName("data")
    var data = CharacterListData()

}

class CharacterListData{

    @SerializedName("total")
    var total = -1

    @SerializedName("offset")
    var offset = -1

    @SerializedName("count")
    var count = -1

    @SerializedName("results")
    var results = listOf<CharacterListItem>()
}

class CharacterListItem {

    @SerializedName("id")
    var id = 0L

    @SerializedName("name")
    var name = ""

    @SerializedName("description")
    var description = ""

    @SerializedName("resourceURI")
    var resourceURI = ""

    @SerializedName("thumbnail")
    var thumbnail = Thumbnail()

}


