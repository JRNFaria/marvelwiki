package com.example.josefaria.marvelwiki.views

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.josefaria.marvelwiki.R
import com.example.josefaria.marvelwiki.models.retrofit.objects.ComicEventStorySeriesListItem
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.utils.Utils
import com.example.josefaria.marvelwiki.viewModels.CharacterDetailViewModel
import iammert.com.expandablelib.ExpandableLayout
import kotlinx.android.synthetic.main.activity_character_detail.*
import kotlinx.android.synthetic.main.view_connection_failed.*
import kotlinx.android.synthetic.main.view_expandable_detail_item.view.*
import kotlinx.android.synthetic.main.view_expandable_detail_parent.view.*
import kotlinx.android.synthetic.main.view_loading_screen.*
import kotlinx.android.synthetic.main.view_pagination_loading_screen.*


class CharacterDetailView : AppCompatActivity() {

    var counter=0
    private val viewModel by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        ViewModelProviders.of(this).get(CharacterDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right)
        setContentView(R.layout.activity_character_detail)

        val id = intent.getLongExtra(Constants.ID_PARAM, -1L)

        if (id == -1L) {
            onBackPressed()
        }

        viewModel.activityState.observeForever { state ->
            if (state != null) {
                when (state) {
                    Constants.INITIAL_LOADING -> {
                        loading_bar.visibility = View.VISIBLE
                        failed_connect.visibility = View.GONE
                        character_card_view.visibility = View.GONE
                    }
                    Constants.INITIAL_LOADED -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.GONE
                        character_card_view.visibility = View.VISIBLE
                        pagination_loading_bar.visibility = View.VISIBLE
                    }
                    Constants.INITIAL_FAILED -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.VISIBLE
                        character_card_view.visibility = View.GONE
                    }
                    Constants.COMICS_FAILED -> {
                        Toast.makeText(this, getString(R.string.title_comics_failed), Toast.LENGTH_SHORT).show()
                        setEndOfLoading()
                    }
                    Constants.EVENTS_FAILED -> {
                        Toast.makeText(this, getString(R.string.title_events_failed), Toast.LENGTH_SHORT).show()
                        setEndOfLoading()
                    }
                    Constants.STORIES_FAILED -> {
                        Toast.makeText(this, getString(R.string.title_stories_failed), Toast.LENGTH_SHORT).show()
                        setEndOfLoading()
                    }
                    Constants.SERIES_FAILED -> {
                        Toast.makeText(this, getString(R.string.title_series_failed), Toast.LENGTH_SHORT).show()
                        setEndOfLoading()
                    }
                    Constants.COMICS_LOADED, Constants.EVENTS_LOADED, Constants.STORIES_LOADED, Constants.SERIES_LOADED ->{
                        setEndOfLoading()
                    }
                }
            }
        }
        retry_connection_button.setOnClickListener {
            viewModel.getCharacter(id)
        }

        viewModel.getCharacter(id)


        setCharacter()
        setExpandableViews()

    }

    //used to show loaded lists
    private fun setEndOfLoading() {
        counter++

        if(counter>3)
        pagination_loading_bar.visibility = View.GONE

    }

    private fun setExpandableViews() {

        val renderer = object : ExpandableLayout.Renderer<String, ComicEventStorySeriesListItem> {
            override fun renderParent(
                view: View,
                category: String,
                isExpanded: Boolean,
                parentPosition: Int
            ) {
                view.section_title.text = category

                view.section_button.setImageDrawable(
                    if (isExpanded)
                        getDrawable(R.drawable.ic_keyboard_arrow_down)
                    else
                        getDrawable(R.drawable.ic_keyboard_arrow_up)
                )
            }

            override fun renderChild(
                view: View,
                item: ComicEventStorySeriesListItem,
                parentPosition: Int,
                childPosition: Int
            ) {

                val requestOptions = Utils.createImageRequestOptions(this@CharacterDetailView)

                view.item_title.text = item.title
                view.item_content.text = if (item.description.isNullOrEmpty())
                    getString(R.string.no_description)
                else
                    item.description

                if (item.thumbnail != null) {
                    Glide.with(this@CharacterDetailView)
                        .load(
                            "${item.thumbnail!!.path}.${item.thumbnail!!.extension}".replace(
                                "http",
                                "https"
                            )
                        )
                        .apply(requestOptions)
                        .into(view.item_image)
                }
                //(view.findViewById(R.id.tvChild) as TextView).setText(model.name)
            }
        }

        expandable_views.setRenderer(renderer)
        expandable_views.setExpandListener { _, _: String, view ->
            view.section_button.setImageDrawable(getDrawable(R.drawable.ic_keyboard_arrow_down))
        }
        expandable_views.setCollapseListener { _, _: String, view ->
            view.section_button.setImageDrawable(getDrawable(R.drawable.ic_keyboard_arrow_up))
        }


        setComics()
        setEvents()
        setSeries()
        setStories()
    }


    private fun setCharacter() {
        viewModel.favourited.observeForever { favourited ->
            if (favourited == true) {
                character_favourite.setImageDrawable(this.getDrawable(R.drawable.ic_star))
            } else {
                character_favourite.setImageDrawable(this.getDrawable(R.drawable.ic_star_border))
            }
        }

        character_favourite.setOnClickListener { viewModel.onFavouritePressed() }
        viewModel.characterInfo.observeForever { characterInfo ->
            if (characterInfo != null) {
                Glide.with(this)
                    .load(
                        "${characterInfo.thumbnail.path}.${characterInfo.thumbnail.extension}".replace(
                            "http",
                            "https"
                        )
                    )
                    .apply(RequestOptions().override(400, 400))
                    .into(character_image)
                character_name.text = characterInfo.name

                character_description.text =
                    if (characterInfo.description.isNotEmpty())
                        characterInfo.description
                    else
                        getString(R.string.no_description)

                character_uri.text = characterInfo.resourceURI

            }
        }
    }

    private fun setComics() {


        viewModel.comicsInfo.observeForever { comics ->
            if (comics != null) {
                expandable_views.addSection(comics)
            }
        }
    }


    private fun setEvents() {
        viewModel.eventsInfo.observeForever { events ->
            if (events != null) {
                expandable_views.addSection(events)
            }
        }
    }


    private fun setStories() {
        viewModel.storiesInfo.observeForever { stories ->
            if (stories != null) {
                expandable_views.addSection(stories)
            }
        }
    }

    private fun setSeries() {
        viewModel.seriesInfo.observeForever { series ->
            if (series != null) {
                expandable_views.addSection(series)
            }
        }
    }


    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)
    }
}
