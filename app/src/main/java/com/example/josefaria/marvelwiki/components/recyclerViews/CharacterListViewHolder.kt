package com.example.josefaria.marvelwiki.components.recyclerViews

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.josefaria.marvelwiki.R
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Utils
import kotlinx.android.synthetic.main.view_character_list_item.view.*

class CharacterListViewHolder(private var parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.view_character_list_item, parent, false)
) {
    /**
     * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
     * ViewHolder when Item is loaded.
     */
    fun bindTo(
        character: Favourite,
        favourites: MutableList<Favourite>?,
        characterListInterface: CharacterListInterface
    ) {

        setImageAndText(character, characterListInterface)

        //if in favourites
        if (favourites != null && favourites.find { it.id == character.id } != null) {
            itemView.character_favourite.setImageDrawable(parent.context.getDrawable(R.drawable.ic_star))
        } else {
            itemView.character_favourite.setImageDrawable(parent.context.getDrawable(R.drawable.ic_star_border))
        }


        itemView.character_favourite.setOnClickListener {
            characterListInterface.favouritePressed(character)
        }
    }


    fun bindTo(
        character: Favourite,
        characterListInterface: CharacterListInterface
    ) {

        setImageAndText(character, characterListInterface)

        itemView.character_favourite.setImageDrawable(parent.context.getDrawable(R.drawable.ic_star))

        itemView.character_favourite.setOnClickListener {
            characterListInterface.favouritePressed(character)
        }
    }

    private fun setImageAndText(
        character: Favourite,
        characterListInterface: CharacterListInterface
    ) {
        itemView.character_name.text = character.name


        val requestOptions =  Utils.createImageRequestOptions(parent.context)

        Glide.with(parent.context)
            .load(character.image)
            .apply(requestOptions)
            .into(itemView.character_image)

        itemView.setOnClickListener {
            characterListInterface.openCharacter(character.id)
        }
    }

}


