package com.example.josefaria.marvelwiki.models.retrofit.services

import com.example.josefaria.marvelwiki.models.retrofit.objects.CharacterListObject
import com.example.josefaria.marvelwiki.models.retrofit.objects.ComicEventStorySeriesListObject
import com.example.josefaria.marvelwiki.utils.Constants
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MarvelAPIService {

    @GET("/v1/public/characters")
    fun getCharacters(
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = "",
        @Query("orderBy") orderBy: String = Constants.NAME_ASC_SORT,
        @Query("nameStartsWith") nameStart: String?,
        @Query("offset") offset: Int = 0
        // @Query("limit") limit: Int = 20
    ): Single<CharacterListObject>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacter(
        @Path("characterId") characterId: Long,
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = ""
    ): Single<CharacterListObject>

    @GET("/v1/public/characters/{characterId}/comics")
    fun getComics(
        @Path("characterId") characterId: Long,
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = "",
        @Query("orderBy") orderBy: String = Constants.SALE_DATE_ASC,
        @Query("limit") limit: Int = 3,
        @Query("formatType") formatType: String = "comic"

    ): Single<ComicEventStorySeriesListObject>

    @GET("/v1/public/characters/{characterId}/events")
    fun getEvents(
        @Path("characterId") characterId: Long,
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = "",
        @Query("orderBy") orderBy: String = Constants.START_DATE_ASC,
        @Query("limit") limit: Int = 3
    ): Single<ComicEventStorySeriesListObject>



    @GET("/v1/public/characters/{characterId}/stories")
    fun getStories(
        @Path("characterId") characterId: Long,
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = "",
        @Query("limit") limit: Int = 3
    ): Single<ComicEventStorySeriesListObject>

    @GET("/v1/public/characters/{characterId}/series")
    fun getSeries(
        @Path("characterId") characterId: Long,
        @Query("apikey") apiKey: String = "",
        @Query("ts") timestamp: String = "",
        @Query("hash") hash: String = "",
        @Query("orderBy") orderBy: String = Constants.START_YEAR_ASC,
        @Query("limit") limit: Int = 3
    ): Single<ComicEventStorySeriesListObject>


    companion object Factory {

        @Volatile
        private var INSTANCE: MarvelAPIService? = null


        fun create(): MarvelAPIService {
            return INSTANCE ?: synchronized(this) {
                // Create database here
                val retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                val instance = retrofit.create(MarvelAPIService::class.java)
                INSTANCE = instance
                instance
            }
        }
    }
}