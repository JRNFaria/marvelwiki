package com.example.josefaria.marvelwiki.components.pagination

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.ItemKeyedDataSource
import android.util.Log
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.models.retrofit.objects.CharacterListItem
import com.example.josefaria.marvelwiki.models.retrofit.services.MarvelAPIService
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.utils.NetworkState
import com.example.josefaria.marvelwiki.utils.Utils
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers


class CharactersDataSource(
    private val compositeDisposable: CompositeDisposable
) : ItemKeyedDataSource<Int, Favourite>() {

    private val marvelApiService = MarvelAPIService.create()
    private var offset = 0

    //network state
    val networkState = MutableLiveData<NetworkState>()

    //end achieved
    val endedList = MutableLiveData<Boolean>()


    /**
     * Keep Completable reference for the retry event
     */
    private var retryCompletable: Completable? = null

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Favourite>) {

        networkState.postValue(NetworkState.LOADING)

        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApiService.getCharacters(
                Constants.PUBLIC_KEY,
                timestamp,
                hash,
                convertedSort(),
                Repository.query,
                offset
            ).subscribe({ characterListObject ->
                networkState.postValue(NetworkState.LOADED)
                offset = characterListObject.data.offset + characterListObject.data.count
                if(offset>=characterListObject.data.total)
                {
                    endedList.postValue(true)
                }
                callback.onResult(convertFromCharacterItemToFavourite(characterListObject.data.results))
            }, { throwable ->
                Log.e("error", throwable.message)
                // keep a Completable for future retry
                setRetry(Action { loadInitial(params, callback) })
                val error = NetworkState.error(throwable.message)
                // publish the error
                networkState.postValue(error)
            })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Favourite>) {
        //get the users from the api after id

        networkState.postValue(NetworkState.LOADING)
        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApiService.getCharacters(
                Constants.PUBLIC_KEY,
                timestamp,
                hash,
                convertedSort(),
                Repository.query,
                offset
            ).subscribe({ characterListObject ->
                networkState.postValue(NetworkState.LOADED)
                offset = characterListObject.data.offset + characterListObject.data.count
                if(offset>=characterListObject.data.total)
                {
                    endedList.postValue(true)
                }
                callback.onResult(convertFromCharacterItemToFavourite(characterListObject.data.results))
            }, { throwable ->
                // keep a Completable for future retry
                setRetry(Action { loadAfter(params, callback) })
                val error = NetworkState.error(throwable.message)
                // publish the error
                networkState.postValue(error)
                // publish the error
                networkState.postValue(NetworkState.error(throwable.message))
            })
        )
    }

    private fun convertFromCharacterItemToFavourite(results: List<CharacterListItem>): List<Favourite> {
        val returnList = mutableListOf<Favourite>()

        results.forEach {
            val imagePath = "${it.thumbnail.path}.${it.thumbnail.extension}".replace("http", "https")
            val favourite = Favourite(it.id, it.name, imagePath)
            returnList.add(favourite)
        }

        return returnList
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Favourite>) {
        // ignored, since we only ever append to our initial load
    }

    override fun getKey(item: Favourite): Int {
        return item.id.toInt()
    }

    private fun setRetry(action: Action?) {
        if (action == null) {
            this.retryCompletable = null
        } else {
            this.retryCompletable = Completable.fromAction(action)
        }
    }

    fun convertedSort(): String = if (Repository.sort == Constants.ROOM_ASC) {
        Constants.NAME_ASC_SORT
    } else {
        Constants.NAME_DESC_SORT
    }


    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(
                retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ }, { throwable -> Log.e("error", throwable.message) })
            )
        }
    }

}