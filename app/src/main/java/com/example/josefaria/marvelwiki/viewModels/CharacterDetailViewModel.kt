package com.example.josefaria.marvelwiki.viewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.models.retrofit.objects.CharacterListItem
import com.example.josefaria.marvelwiki.models.retrofit.objects.ComicEventStorySeriesListItem
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import iammert.com.expandablelib.Section
import io.reactivex.disposables.CompositeDisposable
import com.example.josefaria.marvelwiki.R

class CharacterDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val compositeDisposable = CompositeDisposable()
    private var repository = Repository(application.applicationContext)

    var activityState = MutableLiveData<@Constants.ActivityStates Int>()
    var characterInfo = MutableLiveData<CharacterListItem>()


    var comicsInfo = MutableLiveData<Section<String, ComicEventStorySeriesListItem>>()
    var eventsInfo =MutableLiveData<Section<String, ComicEventStorySeriesListItem>>()
    var storiesInfo = MutableLiveData<Section<String, ComicEventStorySeriesListItem>>()
    var seriesInfo =MutableLiveData<Section<String, ComicEventStorySeriesListItem>>()

    var favourited = MutableLiveData<Boolean>()

    init {
        characterInfo.value = null
        comicsInfo.value = null
        eventsInfo.value = null
        storiesInfo.value = null
        seriesInfo.value = null
        favourited.value = null
    }

    fun getCharacter(id: Long) {
        activityState.value = Constants.INITIAL_LOADING


        repository.getFavourite(id,
            compositeDisposable,
            { favourites ->
                favourited.value = !favourites.isNullOrEmpty()
            },
            { error -> Log.e("error", "Unable to get favourites", error) })



        repository.getCharacter(id, compositeDisposable, { characterList ->
            activityState.value = Constants.INITIAL_LOADED
            getComics(id)
            getEvents(id)
            getStories(id)
            getSeries(id)
            characterInfo.postValue(characterList.data.results[0])
        }, {
            activityState.value = Constants.INITIAL_FAILED
        })
    }

    private fun getEvents(id: Long) {
        activityState.value = Constants.EVENTS_LOADING
        repository.getEvents(id, compositeDisposable, { events ->
            activityState.value = Constants.EVENTS_LOADED

            if(events.data.results.isNotEmpty()) {
                val category = getApplication<Application>().applicationContext.getString(R.string.title_last_events,events.data.results.size)

                val section = Section<String, ComicEventStorySeriesListItem>()

                events.data.results.forEach {
                    section.parent = category
                    section.children.add(it)
                }
                eventsInfo.postValue(section)
            }
        }, { activityState.value = Constants.EVENTS_FAILED })
    }


    private fun getComics(id: Long) {
        activityState.value = Constants.COMICS_LOADING
        repository.getComics(id, compositeDisposable, { comics ->
            activityState.value = Constants.COMICS_LOADED

            if(comics.data.results.isNotEmpty()) {
                val category = getApplication<Application>().applicationContext.getString(R.string.title_last_comics,comics.data.results.size)
                val section = Section<String, ComicEventStorySeriesListItem>()


                comics.data.results.forEach {
                    section.parent = category
                    section.children.add(it)
                }
                comicsInfo.postValue(section)

            }
        }, { activityState.value = Constants.COMICS_FAILED })
    }


    private fun getStories(id: Long) {
        activityState.value = Constants.STORIES_LOADING
        repository.getStories(id, compositeDisposable, { stories ->
            activityState.value = Constants.STORIES_LOADED
            if(stories.data.results.isNotEmpty()) {
                val category = getApplication<Application>().applicationContext.getString(R.string.title_last_stories,stories.data.results.size)
                val section = Section<String, ComicEventStorySeriesListItem>()

                stories.data.results.forEach {
                    section.parent = category
                    section.children.add(it)
                }
                storiesInfo.postValue(section)
            }
        }, { activityState.value = Constants.STORIES_FAILED })
    }


    private fun getSeries(id: Long) {
        activityState.value = Constants.SERIES_LOADING
        repository.getSeries(id, compositeDisposable, { series ->
            activityState.value = Constants.SERIES_LOADED
            if(series.data.results.isNotEmpty()) {
                val category = getApplication<Application>().applicationContext.getString(R.string.title_last_series,series.data.results.size)

                val section = Section<String, ComicEventStorySeriesListItem>()

                series.data.results.forEach {
                    section.parent = category
                    section.children.add(it)
                }
                seriesInfo.postValue(section)
            }
        }, { throwable ->
            Log.d("error", throwable.message)
            activityState.value = Constants.SERIES_FAILED
        })
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun onFavouritePressed() {
        if (favourited.value != null && characterInfo.value != null) {
            val characterItem = characterInfo.value!!
            val imagePath =
                "${characterItem.thumbnail.path}.${characterItem.thumbnail.extension}".replace("http", "https")
            val favourite = Favourite(characterItem.id, characterItem.name, imagePath)

            if (favourited.value == true) {
                repository.deleteFavourite(favourite)
            } else {
                repository.insertFavourite(favourite)
            }
        }
    }
}

