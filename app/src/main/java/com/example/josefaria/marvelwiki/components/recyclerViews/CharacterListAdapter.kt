package com.example.josefaria.marvelwiki.components.recyclerViews

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import com.example.josefaria.marvelwiki.models.room.Favourite

class CharacterListAdapter(private val characterList: CharacterListInterface, private var checkFavourites: Boolean) :
    PagedListAdapter<Favourite, CharacterListViewHolder>(CharacterDiffCallback) {

    private var favourites: MutableList<Favourite>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterListViewHolder {
        return CharacterListViewHolder(parent)
    }

    override fun onBindViewHolder(holder: CharacterListViewHolder, position: Int) {
        if (checkFavourites)
            holder.bindTo(this.getItem(position)!!, favourites, characterList)
        else {
            holder.bindTo(this.getItem(position)!!, characterList)
        }
    }

    fun setListOfFavourites(newFavourites: MutableList<Favourite>) {
        favourites = newFavourites
        notifyDataSetChanged()
    }

    companion object {
        val CharacterDiffCallback = object : DiffUtil.ItemCallback<Favourite>() {
            override fun areItemsTheSame(oldItem: Favourite, newItem: Favourite): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Favourite, newItem: Favourite): Boolean {
                return oldItem == newItem
            }
        }
    }

}

