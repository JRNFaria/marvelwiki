package com.example.josefaria.marvelwiki.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.josefaria.marvelwiki.R
import com.example.josefaria.marvelwiki.components.recyclerViews.CharacterListAdapter
import com.example.josefaria.marvelwiki.components.recyclerViews.CharacterListInterface
import com.example.josefaria.marvelwiki.components.recyclerViews.SpacesItemDecoration
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.viewModels.FavouritesListViewModel
import kotlinx.android.synthetic.main.fragment_character_list.*
import kotlinx.android.synthetic.main.view_loading_screen.*
import kotlinx.android.synthetic.main.view_no_results.*

class FavouritesListFragment : Fragment(),
    CharacterListInterface {

    private val viewModel by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        ViewModelProviders.of(this).get(FavouritesListViewModel::class.java)
    }
    private var fragmentListener: ListFragmentListener? = null
    private lateinit var observer: Observer<PagedList<Favourite>>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        character_list.layoutManager = GridLayoutManager(this.context, 2)

        setListAdapter()

        character_list.addItemDecoration(
            SpacesItemDecoration(2)
        )

        viewModel.activityState.observeForever { state ->
            if (state != null) {
                when (state) {
                    Constants.INITIAL_LOADING -> {
                        loading_bar.visibility = View.VISIBLE
                        no_results.visibility = View.GONE
                    }
                    Constants.INITIAL_LOADED -> {
                        loading_bar.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun setListAdapter() {
        val adapter = CharacterListAdapter(this, false)

        observer = Observer {
            adapter.submitList(it)
            if (it == null) {
                viewModel.activityState.postValue(Constants.INITIAL_LOADING)
            } else {
                viewModel.activityState.postValue(Constants.INITIAL_LOADED)

                if (it.isEmpty()) {
                    character_list.visibility = View.GONE
                    no_results.visibility = View.VISIBLE

                } else {
                    character_list.visibility = View.VISIBLE
                    no_results.visibility = View.GONE
                }
            }

        }

        viewModel.listOfCharacters.observe(this, observer)
        character_list.adapter = adapter
    }

    override fun onDestroyView() {
        fragmentListener = null
        super.onDestroyView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_character_list, container, false)
    }

    override fun openCharacter(id: Long) {
        fragmentListener?.openCharacter(id)
    }

    override fun favouritePressed(favourite: Favourite) {
        viewModel.favouritePressed(favourite)
    }

    fun setListener(newFragmentListener: ListFragmentListener) {
        fragmentListener = newFragmentListener
    }

    fun datasetChanged() {
        viewModel.datasetChanged()
        viewModel.listOfCharacters.removeObserver(observer)
        setListAdapter()
    }

    companion object {
        fun newInstance(): FavouritesListFragment = FavouritesListFragment()
    }
}
