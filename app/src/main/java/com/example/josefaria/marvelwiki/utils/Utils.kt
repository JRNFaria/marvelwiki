package com.example.josefaria.marvelwiki.utils

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.util.Log
import com.bumptech.glide.request.RequestOptions
import java.math.BigInteger
import java.security.MessageDigest

class Utils {

    companion object {

        private const val ALGORITHM_MD5 = "MD5"

        /**
         * Returns a string with the generated hash encrypted in MD5
         * to make request to the marvel api service
         * @param  timestamp  a timestamp string with the system time
         * @param  privateKey private key of your marvel api service account
         * @param  publicKey public key of the marvel api service
         * @return string of the generated hash
         */
        @JvmStatic
        fun generateHash(timestamp: String, privateKey: String, publicKey: String): String {

            val value = timestamp + privateKey + publicKey
            val md5Encoder = MessageDigest.getInstance(ALGORITHM_MD5)
            val returnValue = BigInteger(1, md5Encoder.digest(value.toByteArray())).toString(16).padStart(32, '0')

            Log.d("restHash", returnValue)
            return returnValue
        }

        /**
         *
         */
        @JvmStatic
        fun createImageRequestOptions(context: Context): RequestOptions {

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            return RequestOptions()
                .placeholder(circularProgressDrawable)
                .override(400, 400)
                .fitCenter()
        }




    }
}