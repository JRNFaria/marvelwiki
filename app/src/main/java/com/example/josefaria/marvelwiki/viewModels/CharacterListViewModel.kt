package com.example.josefaria.marvelwiki.viewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.util.Log
import com.example.josefaria.marvelwiki.components.pagination.CharacterDataSourceFactory
import com.example.josefaria.marvelwiki.components.pagination.CharactersDataSource
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.utils.NetworkState
import com.example.josefaria.marvelwiki.utils.Status
import io.reactivex.disposables.CompositeDisposable


class CharacterListViewModel(application: Application) : AndroidViewModel(application) {
    private val compositeDisposable = CompositeDisposable()
    private var repository = Repository(application.applicationContext)
    var activityState = MutableLiveData<@Constants.ActivityStates Int>()

    private val sourceFactory =
        CharacterDataSourceFactory(compositeDisposable)
    var listOfCharacters = repository.getRetrofitPagination(sourceFactory)
    var listOfFavourites = MutableLiveData<MutableList<Favourite>>()


    private var networkState: LiveData<NetworkState> = Transformations.switchMap<CharactersDataSource, NetworkState>(
        sourceFactory.usersDataSourceLiveData
    ) { it.networkState }

    var listEnded: LiveData<Boolean> = Transformations.switchMap<CharactersDataSource, Boolean>(
        sourceFactory.usersDataSourceLiveData
    ) { it.endedList }

    init {
        activityState.value = Constants.INITIAL_LOADING
        listOfFavourites.value = null
        getFavourites()

        networkState.observeForever { newNetworkState ->
            if (newNetworkState != null) {
                when (newNetworkState.status) {
                    Status.RUNNING -> {
                        if (activityState.value == Constants.INITIAL_LOADED ||activityState.value == Constants.PAGINATION_FAILED) {
                            activityState.value=Constants.PAGINATION_LOADING
                        } else {
                            activityState.value=Constants.INITIAL_LOADING
                        }
                    }
                    Status.FAILED -> {

                        if (activityState.value == Constants.INITIAL_LOADING||activityState.value == Constants.INITIAL_FAILED) {
                            activityState.value=Constants.INITIAL_FAILED
                        } else {
                            activityState.value=Constants.PAGINATION_FAILED
                        }
                    }
                    Status.SUCCESS -> activityState.value = Constants.INITIAL_LOADED

                }
            }
        }
    }


    private fun getFavourites() {
        repository.getFavourites(
            compositeDisposable,
            { listOfFavourites.postValue(it.toMutableList()) },
            { error -> Log.e("error", "Unable to get favourites", error) })
    }

    fun favouritePressed(favourite: Favourite) {
        if (listOfFavourites.value!!.contains(favourite)) {
            repository.deleteFavourite(favourite)
        } else {
            repository.insertFavourite(favourite)
        }
    }

    fun retry() {
        sourceFactory.usersDataSourceLiveData.value!!.retry()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun datasetChanged() {
        sourceFactory.datasetChanged()
    }

}