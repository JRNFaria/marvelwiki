package com.example.josefaria.marvelwiki.models.retrofit.objects

import com.google.gson.annotations.SerializedName

class Thumbnail {

    @SerializedName("path")
    var path = ""

    @SerializedName("extension")
    var extension = ""
}