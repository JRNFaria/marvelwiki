package com.example.josefaria.marvelwiki.views

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import com.example.josefaria.marvelwiki.R
import com.example.josefaria.marvelwiki.components.viewPagers.MainViewPager
import com.example.josefaria.marvelwiki.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*


class MainView : AppCompatActivity(), ListFragmentListener {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_characters -> {
                view_pager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favourites -> {
                view_pager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            else -> {
                view_pager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.josefaria.marvelwiki.R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        initViewPager()
    }

    private fun initViewPager() {
        view_pager.adapter = MainViewPager(this.applicationContext, supportFragmentManager, this)
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                navigation.menu.getItem(position).isChecked = true
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_character_list, menu)

        // Associate searchable configuration with the SearchView
        val searchView = menu.findItem(R.id.menu_search).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = true
            override fun onQueryTextChange(query: String?): Boolean {
                (view_pager.adapter as MainViewPager).search(query)
                return true
            }
        })
        return true
    }

    override fun openCharacter(id: Long) {
        val intent = Intent(this, CharacterDetailView::class.java)
        intent.putExtra(Constants.ID_PARAM, id)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_sort -> {
                (view_pager.adapter as MainViewPager).sort()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
