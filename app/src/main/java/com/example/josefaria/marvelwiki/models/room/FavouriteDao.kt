package com.example.josefaria.marvelwiki.models.room

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao

interface FavouriteDao {

    @Query("SELECT * from favourites ORDER BY name ASC")
    fun getFavourites(): Flowable<List<Favourite>>

    @Query("SELECT * from favourites WHERE name LIKE :query ORDER BY CASE WHEN :sort = 1 THEN name END ASC, CASE WHEN :sort = 2 THEN name END DESC")
    fun getFavouritesPaged(sort: Int, query: String):  DataSource.Factory<Int, Favourite>

    @Query("SELECT * from favourites WHERE name LIKE :query ORDER BY CASE WHEN :sort = 1 THEN name END ASC, CASE WHEN :sort = 2 THEN name END DESC")
    fun getFavouritesPagedDirectly(sort: Int, query: String):List<Favourite>

    @Query("SELECT * from favourites ORDER BY name ASC")
    fun getFavouritesPagedDesc():  DataSource.Factory<Int, Favourite>

    @Query("SELECT * from favourites WHERE favourites.id=:id")
    fun getFavourite(id: Long): Flowable<List<Favourite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favourite: Favourite)

    @Delete
    fun delete(favourite: Favourite)

    @Query("DELETE FROM favourites")
    fun deleteAll()


}