package com.example.josefaria.marvelwiki.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.josefaria.marvelwiki.R
import com.example.josefaria.marvelwiki.components.recyclerViews.CharacterListAdapter
import com.example.josefaria.marvelwiki.components.recyclerViews.CharacterListInterface
import com.example.josefaria.marvelwiki.components.recyclerViews.SpacesItemDecoration
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.viewModels.CharacterListViewModel
import kotlinx.android.synthetic.main.fragment_character_list.*
import kotlinx.android.synthetic.main.view_connection_failed.*
import kotlinx.android.synthetic.main.view_loading_screen.*
import kotlinx.android.synthetic.main.view_no_results.*
import kotlinx.android.synthetic.main.view_pagination_connection_failed.*
import kotlinx.android.synthetic.main.view_pagination_loading_screen.*

class CharacterListFragment : Fragment(),
    CharacterListInterface {

    private val viewModel by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        ViewModelProviders.of(this).get(CharacterListViewModel::class.java)
    }

    private var fragmentListener: ListFragmentListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        character_list.layoutManager = GridLayoutManager(this.context, 2)


        val adapter = CharacterListAdapter(this, true)
        character_list.adapter = adapter

        character_list.addItemDecoration(
            SpacesItemDecoration(2)
        )

        viewModel.listEnded.observeForever { listEnded ->
            if (listEnded == true) {
                Toast.makeText(context, getString(R.string.no_more_results), Toast.LENGTH_SHORT)
                    .show()
            }

        }
        viewModel.listOfCharacters.observe(this, Observer<PagedList<Favourite>> {
            adapter.submitList(it)

            if (it != null) {
                if (it.isEmpty()) {
                    no_results.visibility = View.VISIBLE
                } else {
                    no_results.visibility = View.GONE
                }
            }
        })

        viewModel.activityState.observeForever { state ->
            if (state != null) {
                when (state) {
                    Constants.INITIAL_LOADING -> {
                        loading_bar.visibility = View.VISIBLE
                        failed_connect.visibility = View.INVISIBLE
                        pagination_failed_connect.visibility = View.GONE
                        pagination_loading_bar.visibility = View.GONE
                    }
                    Constants.INITIAL_LOADED -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.INVISIBLE
                        pagination_failed_connect.visibility = View.GONE
                        character_list.visibility = View.VISIBLE
                        pagination_loading_bar.visibility = View.GONE
                    }
                    Constants.INITIAL_FAILED -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.VISIBLE
                        pagination_failed_connect.visibility = View.GONE
                        character_list.visibility = View.GONE
                        pagination_loading_bar.visibility = View.GONE
                    }

                    Constants.PAGINATION_LOADING -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.GONE
                        pagination_failed_connect.visibility = View.GONE
                        character_list.visibility = View.VISIBLE
                        pagination_loading_bar.visibility = View.VISIBLE
                    }

                    Constants.PAGINATION_FAILED -> {
                        loading_bar.visibility = View.GONE
                        failed_connect.visibility = View.GONE
                        pagination_failed_connect.visibility = View.VISIBLE
                        character_list.visibility = View.VISIBLE
                        pagination_loading_bar.visibility = View.GONE
                    }

                }
            }
        }

        retry_connection_button.setOnClickListener {
            viewModel.retry()
        }

        retry_connection_pagination_button.setOnClickListener {
            viewModel.retry()
        }

        viewModel.listOfFavourites.observeForever { favourites ->
            if (favourites != null) {
                (character_list.adapter as CharacterListAdapter).setListOfFavourites(favourites)
            }
        }
    }

    override fun onDestroyView() {
        fragmentListener = null
        super.onDestroyView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_character_list, container, false)
    }

    fun datasetChanged() {
        viewModel.datasetChanged()
    }

    fun setListener(newFragmentListener: ListFragmentListener) {
        fragmentListener = newFragmentListener
    }

    override fun openCharacter(id: Long) {
        fragmentListener?.openCharacter(id)
    }

    override fun favouritePressed(favourite: Favourite) {
        viewModel.favouritePressed(favourite)
    }

    companion object {
        fun newInstance(): CharacterListFragment = CharacterListFragment()
    }
}

interface ListFragmentListener {
    fun openCharacter(id: Long)
}