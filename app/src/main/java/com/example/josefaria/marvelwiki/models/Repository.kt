package com.example.josefaria.marvelwiki.models

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.content.Context
import com.example.josefaria.marvelwiki.components.pagination.CharacterDataSourceFactory
import com.example.josefaria.marvelwiki.models.retrofit.objects.CharacterListObject
import com.example.josefaria.marvelwiki.models.retrofit.objects.ComicEventStorySeriesListObject
import com.example.josefaria.marvelwiki.models.retrofit.services.MarvelAPIService
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.models.room.FavouriteDatabase
import com.example.josefaria.marvelwiki.utils.Constants
import com.example.josefaria.marvelwiki.utils.Utils
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class Repository(context: Context) {


    //var database access
    private var database = FavouriteDatabase.getDatabase(context)
    private var favouriteDao = database.favouriteDao()

    //var retrofit

    private var marvelApi = MarvelAPIService.create()

    companion object {
        var sort = Constants.ROOM_ASC
        var query = "%"

        @JvmStatic
        fun changeSort() {
            sort = if (sort == Constants.ROOM_ASC)
                Constants.ROOM_DESC
            else
                Constants.ROOM_ASC
        }

        @JvmStatic
        fun search(newQuery: String?) {
            query = if (newQuery == null || newQuery.isEmpty()) {
                "%"
            } else {
                "$newQuery%"
            }
        }
    }

    private val config = PagedList.Config.Builder()
        .setPageSize(Constants.NUMBER_ITEMS_PER_PAGE)
        .setInitialLoadSizeHint(Constants.NUMBER_ITEMS_PER_PAGE)
        .setEnablePlaceholders(false)
        .build()

    //Room operations----------------------------------------------------------------------------------------------------------------------------
    fun getFavourites(
        compositeDisposable: CompositeDisposable,
        onComplete: ((t: List<Favourite>) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {
        compositeDisposable.add(
            favouriteDao.getFavourites().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }

    fun deleteFavourite(
        favourite: Favourite
    ) {

        Completable.fromAction { favouriteDao.delete(favourite) }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onComplete() {
                    // getFavourites()
                }

                override fun onError(e: Throwable) {
                }
            })
    }

    fun insertFavourite(
        favourite: Favourite
    ) {
        Completable.fromAction { favouriteDao.insert(favourite) }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onComplete() {
                    // getFavourites()
                }

                override fun onError(e: Throwable) {
                }
            })
    }


    fun getFavouritePagination(factory: DataSource.Factory<Int, Favourite>) =
        LivePagedListBuilder<Int, Favourite>(factory, config).build()

    fun getFavouriteFactory(): DataSource.Factory<Int, Favourite> = favouriteDao.getFavouritesPaged(sort, query)

    fun getFavourite(
        id: Long,
        compositeDisposable: CompositeDisposable,
        onComplete: ((t: List<Favourite>) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {
        compositeDisposable.add(

            favouriteDao.getFavourite(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }
    //Room tests--------------------------------------------------------------------------------------------------------------------------------------

    fun getFavourites(): List<Favourite> {
        return favouriteDao.getFavouritesPagedDirectly(sort, query)
    }

    fun insertFavouriteDirectly(favourite: Favourite) {
        favouriteDao.insert(favourite)
    }


    //Retrofit operations----------------------------------------------------------------------------------------------------------------------------

    fun getRetrofitPagination(sourceFactory: CharacterDataSourceFactory): LiveData<PagedList<Favourite>> {
        return LivePagedListBuilder<Int, Favourite>(sourceFactory, config).build()
    }

    fun getCharacter(
        id: Long, compositeDisposable: CompositeDisposable, onComplete: ((CharacterListObject) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {
        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )

        compositeDisposable.add(
            marvelApi.getCharacter(
                id,
                Constants.PUBLIC_KEY,
                timestamp,
                hash
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }

    fun getEvents(
        id: Long, compositeDisposable: CompositeDisposable, onComplete: ((ComicEventStorySeriesListObject) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {

        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApi.getEvents(
                id,
                Constants.PUBLIC_KEY,
                timestamp,
                hash,
                Constants.START_DATE_ASC
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }

    fun getComics(
        id: Long, compositeDisposable: CompositeDisposable, onComplete: ((ComicEventStorySeriesListObject) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {

        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApi.getComics(
                id,
                Constants.PUBLIC_KEY,
                timestamp,
                hash,
                Constants.SALE_DATE_ASC
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }

    fun getStories(
        id: Long, compositeDisposable: CompositeDisposable, onComplete: ((ComicEventStorySeriesListObject) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {

        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApi.getStories(
                id,
                Constants.PUBLIC_KEY,
                timestamp,
                hash
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }

    fun getSeries(
        id: Long, compositeDisposable: CompositeDisposable, onComplete: ((ComicEventStorySeriesListObject) -> Unit),
        onError: ((t: Throwable) -> Unit)
    ) {

        val timestamp = System.currentTimeMillis().toString()
        val hash = Utils.generateHash(
            timestamp,
            Constants.PRIVATE_KEY,
            Constants.PUBLIC_KEY
        )
        compositeDisposable.add(
            marvelApi.getSeries(
                id,
                Constants.PUBLIC_KEY,
                timestamp,
                hash,
                Constants.START_YEAR_ASC
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        )
    }
}