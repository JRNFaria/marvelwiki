package com.example.josefaria.marvelwiki.components.viewPagers

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.views.CharacterListFragment
import com.example.josefaria.marvelwiki.views.FavouritesListFragment
import com.example.josefaria.marvelwiki.views.ListFragmentListener

class MainViewPager(var context: Context, fm: FragmentManager, private val listener: ListFragmentListener) :
    FragmentStatePagerAdapter(fm) {

    private lateinit var fragment: Fragment
    private val fragments = HashMap<Int,Fragment>()

    companion object {
        const val NUM_ITEMS = 2
    }


    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {

        fragment = when (position) {
            0 -> CharacterListFragment.newInstance()
            1 -> FavouritesListFragment.newInstance()
            else -> CharacterListFragment.newInstance()
        }

        if (fragment is CharacterListFragment) {
            (fragment as CharacterListFragment).setListener(listener)
        } else if (fragment is FavouritesListFragment) {
            (fragment as FavouritesListFragment).setListener(listener)
        }

        fragments[position] = fragment

        return fragment
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    fun search(query: String?) {
        Repository.search(query)
        fragments.forEach {
                (_, fragment) ->
            (fragment as? CharacterListFragment)?.datasetChanged()
            (fragment as? FavouritesListFragment)?.datasetChanged()
        }
    }

    fun sort() {
        //changes sort in repo
        Repository.changeSort()
        fragments.forEach {
                (_, fragment) ->
            (fragment as? CharacterListFragment)?.datasetChanged()
            (fragment as? FavouritesListFragment)?.datasetChanged()
        }
    }

}