package com.example.josefaria.marvelwiki.viewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants

class FavouritesListViewModel(application: Application) : AndroidViewModel(application) {
    private var repository = Repository(application.applicationContext)
    private var factory: DataSource.Factory<Int, Favourite> = repository.getFavouriteFactory()
    var listOfCharacters = repository.getFavouritePagination(factory)

    var activityState = MutableLiveData<@Constants.ActivityStates Int>()


    init{
        activityState.value=Constants.INITIAL_LOADING
    }
    fun favouritePressed(favourite: Favourite) {
        repository.deleteFavourite(favourite)
    }
    fun datasetChanged() {
        factory = repository.getFavouriteFactory()
        listOfCharacters = repository.getFavouritePagination(factory)
    }
}

