package com.example.josefaria.marvelwiki.models.retrofit.objects

import com.google.gson.annotations.SerializedName

class ComicEventStorySeriesListObject {

    @SerializedName("code")
    var code = -1

    @SerializedName("data")
    var data = ComicEventStorySeriesListData()
}

class ComicEventStorySeriesListData{

    @SerializedName("count")
    var count = -1

    @SerializedName("results")
    var results = listOf<ComicEventStorySeriesListItem>()
}

class ComicEventStorySeriesListItem {

    @SerializedName("id")
    var id = 0L

    @SerializedName("title")
    var title = ""


    @SerializedName("description")
    var description :String?= ""

    @SerializedName("thumbnail")
    var thumbnail:Thumbnail? = Thumbnail()

}

