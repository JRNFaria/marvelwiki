package com.example.josefaria.marvelwiki.components.pagination

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.utils.Constants
import io.reactivex.disposables.CompositeDisposable

class CharacterDataSourceFactory(private val compositeDisposable: CompositeDisposable) :
    DataSource.Factory<Int, Favourite>() {

    val usersDataSourceLiveData = MutableLiveData<CharactersDataSource>()

    override fun create(): DataSource<Int, Favourite> {
        val usersDataSource =
            CharactersDataSource(compositeDisposable)
        usersDataSourceLiveData.postValue(usersDataSource)
        return usersDataSource
    }


    fun datasetChanged() {
        usersDataSourceLiveData.value?.invalidate()
    }
}