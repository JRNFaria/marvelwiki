package com.example.josefaria.marvelwiki.utils

import android.support.annotation.IntDef


class Constants {


    companion object {

        const val BASE_URL = "https://gateway.marvel.com"
        const val PUBLIC_KEY = "675c028b1ff123df42d95d6c0d26eb0c"
        const val PRIVATE_KEY = "824a977c3c4170dd5aa1b66d9e91a67446619733"

        //used to characters
        const val NAME_ASC_SORT = "name"
        const val NAME_DESC_SORT = "-name"

        //used to sort characters in ROOM
        const val ROOM_ASC = 1
        const val ROOM_DESC = 2

        //used to sort comics
        const val SALE_DATE_ASC = "onsaleDate"
        const val SALE_DATE_DESC = "-onsaleDate"

        //used to sort events
        const val START_DATE_ASC = "startDate"
        const val START_DATE_DESC = "-startDate"

        //used to sort series
        const val START_YEAR_ASC = "startYear"
        const val START_YEAR_DESC = "-startYear"

        //pagination
        const val NUMBER_ITEMS_PER_PAGE = 20

        //enum
        const val ID_PARAM = "characterIdParam"


        //activities/fragment state machine
        const val INITIAL_LOADING = 1
        const val INITIAL_LOADED = 2
        const val INITIAL_FAILED = 3
        //specific for list fragments
        const val PAGINATION_LOADING = 4
        const val PAGINATION_LOADED = 5
        const val PAGINATION_FAILED = 6
        //specific for character details
        const val COMICS_LOADING = 7
        const val COMICS_LOADED = 8
        const val COMICS_FAILED = 9
        const val EVENTS_LOADING = 10
        const val EVENTS_LOADED = 11
        const val EVENTS_FAILED = 12
        const val STORIES_LOADING = 13
        const val STORIES_LOADED = 14
        const val STORIES_FAILED = 15
        const val SERIES_LOADING = 16
        const val SERIES_LOADED = 17
        const val SERIES_FAILED = 18

        /*Overuse of ENUM in Android will increase DEX size and increase runtime memory allocation size.*/
        // Declare the @IntDef for these constants
    }

    @IntDef(
        INITIAL_LOADING,
        INITIAL_LOADED,
        INITIAL_FAILED,
        PAGINATION_LOADING,
        PAGINATION_LOADED,
        PAGINATION_FAILED,
        COMICS_LOADING,
        COMICS_LOADED,
        COMICS_FAILED,
        EVENTS_LOADING,
        EVENTS_LOADED,
        EVENTS_FAILED,
        STORIES_LOADING,
        STORIES_LOADED,
        STORIES_FAILED,
        SERIES_LOADING,
        SERIES_LOADED,
        SERIES_FAILED
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ActivityStates
}