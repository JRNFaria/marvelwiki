package com.example.josefaria.marvelwiki.models.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

//character id acts as primary key to simplify the database, names is also saved the name and image path to show the user without rest request
@Entity(tableName = "favourites")
data class Favourite(@PrimaryKey @ColumnInfo(name = "id") val id: Long, @ColumnInfo(name = "name") val name: String,@ColumnInfo(name = "image") val image: String)