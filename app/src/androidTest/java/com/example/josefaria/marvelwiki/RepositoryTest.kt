package com.example.josefaria.marvelwiki

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.example.josefaria.marvelwiki.models.Repository
import com.example.josefaria.marvelwiki.models.room.Favourite
import com.example.josefaria.marvelwiki.models.room.FavouriteDatabase
import com.example.josefaria.marvelwiki.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class RepositoryTest {

    private lateinit var repository: Repository
    /*  @Test
      fun useAppContext() {
          // Context of the app under test.
          val appContext = InstrumentationRegistry.getTargetContext()
          assertEquals("com.example.josefaria.marvelwiki", appContext.packageName)
      }*/

    @Before
    fun createRepository() {
        val context = InstrumentationRegistry.getTargetContext()
        FavouriteDatabase.TEST_MODE=true
        repository = Repository(context)
    }

    //test encryption using example in marvel api
    @Test
    fun testEncription() {

        val publicKey = "1234"
        val privateKey = "abcd"
        val timestamp = "1"
        val hash = "ffd275c5130566a2916217b101f26150"

        assertEquals(hash, Utils.generateHash(timestamp, privateKey, publicKey))
    }

    //test insert, delete,sort, search
    @Test
    fun favouritesTest() {
        val favourite1=Favourite(1, "name", "image")
        val favourite2=Favourite(2, "name1", "image1")
        val favourite3=Favourite(3, "search", "image2")
        val favourite4=Favourite(4, "sort", "image3")


        repository.insertFavouriteDirectly(favourite1)
        repository.insertFavouriteDirectly(favourite2)
        repository.insertFavouriteDirectly(favourite3)
        repository.insertFavouriteDirectly(favourite4)

        var list = repository.getFavourites()
        assertEquals(list.size, 4)
        assertEquals(list[0].id, 1)
        assertEquals(list[1].id, 2)
        assertEquals(list[2].id, 3)
        assertEquals(list[3].id, 4)

        repository.changeSort()
        list = repository.getFavourites()
        assertEquals(list.size, 4)

        assertEquals(list[0].id, 4)
        assertEquals(list[1].id, 3)
        assertEquals(list[2].id, 2)
        assertEquals(list[3].id, 1)

        repository.search("search")
        list = repository.getFavourites()
        assertEquals(list.size, 1)
        assertEquals(list[0].id, 3)
    }

    //test insert, delete,sort, search
    @Test
    fun retrofitTest() {
        repository.getFavouriteFactory()
    }

}
