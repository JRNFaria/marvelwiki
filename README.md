# MarvelWiki

Android application that shows list of marvel characters and their corresponding information using the data available in the [marvel api](https://developer.marvel.com).

## Project architecture

The MarvelWiki application follows a MVVM approach with support of the google architecture libraries. All of the application was created using the Kotlin programming language.

The UI of this project can be separated in two different views: the MainView and the CharacterDetailView. The MainView is the first view that the user interacts, and where sees the characters and favourite characters lists.


The second view, CharacterDetailView is the view that is called when the user wants more information about a character and presses the frame of a character in the list shown.

In the background the majority of the operations are made by the ViewModels associated with each view and fragment together with a repository object that abstracts the use of the requests to the server and database in the project.

## Project structure
In this section some more prominent rules about the project and code organization will be explained

### Project folder structure

This project is structured in a similar way to a normal android project since it was created by Android Studio 3.3. The main difference may happen inside the com.example.josefaria.marvelwiki folder (package folder) where 5 folders were created in order to organize the code:

* components: classes related to custom layout components and adapters like viewpagers, recyclers views and pagination
* models: retrofit and room functions
* utils: constants and generic/repeated functions and wrappers
* viewModels: view models
* views: fragments and views classes


### Naming conventions in the Project

Since this project uses direct binding available from kotlin-android-extensions some naming conventions were made in the code of this project in order to facilitate its understanding:

* normal variables names start in lower-case and, if they have more than one word, in the beginning of the new word start in upper-case, e.g: var thisVariable = 0
* layout ids start in lower case and if they have more than a word are separated by underscore, e.g: this_button.setOnClickListener{...}

This may look like simple rules but they can help the code readability, if we look to the two code blocks below it becomes easier to read the first one.
```
character_list.addItemDecoration(SpacesItemDecoration(2))

viewModel.listOfCharacters.observe(this, Observer<PagedList<Favourite>> {
adapter.submitList(it)
})

retry_connection_button.setOnClickListener {
viewModel.retry()
}

```

```
characterList.addItemDecoration(SpacesItemDecoration(2))

viewModel.listOfCharacters.observe(this, Observer<PagedList<Favourite>> {
adapter.submitList(it)
})

retryConnectionButton.setOnClickListener {
viewModel.retry()
}
```


## Libraries
The libraries used in this project in order to facilitate the implementation of the app and its user stories are:
* [Android support libraries](https://developer.android.com/topic/libraries/support-library/features): libraries that helps with the use of lots of things including support for Fragments and the Loader framework.
* [Android lifecycle components](https://developer.android.com/topic/libraries/architecture): libraries that provide classes that need to work with the Android lifecycle and helps to implement a MVVM architecture and pagination load
* [Retrofit2](https://square.github.io/retrofit/): used in order to simplify REST requests to the marvel API service
* [Room](https://developer.android.com/topic/libraries/architecture/room): a persistence library, it makes it easier to work with SQLiteDatabase objects in your app, used in the favourite hero user story to save information of favourite heroes
* [RxJava and RxAndroid](https://github.com/ReactiveX/RxAndroid): used to help create a reactive solution, in this project was used to make the retrofit and room request reactive.
* [Glide](https://github.com/bumptech/glide): to use show and cache images when necessary
* [Leakcanary](https://github.com/square/leakcanary): library used to test application and find out if any memory leaks is happening
* [ExpandableLayout by iammert](https://github.com/iammert/ExpandableLayout): a library used to decrease the time to implement expandable views that show comics/events/series/stories layouts in the view with the character details

## Features (by user story)

### US-1:​ List Heroes
DESCRIPTION:​ As a user I want to see a list of Marvel Heroes.

ACCEPTANCE CRITERIA:
  * List should have pagination of 20 items
  * At each page load, show the user some feedback

BONUS:
  * Inform the user when there are no more results to be listed

In this user story, all the features and bonus requested were completed. The list is shown to the user in the CharacterListFragment inside of the MainView, this user story was completed by mainly using Retrofit, Glide, LiveData, Pagination and RxJava libraries in order to make request to the marvel api service.
The UI of this user story also notifies the user when there are connections problems or the images are still loading.

![MainView](https://bitbucket.org/JRNFaria/marvelwiki/raw/23ed2a55c7dea8825de582002987de49ab43c3a7/screenshots/mainView.jpg)



### US-2: ​Hero Detail
DESCRIPTION: ​As a user I want to see the details of an Hero.
ACCEPTANCE CRITERIA:
* Show the details of an Hero (name, photo, description and resourceURI)
* List the first 3 comics (if any) this hero takes part in with name and description
of the comics.
* List the first 3 events (if any) this hero takes part in with name and description
* List the first 3 stories (if any) this hero takes part in with name and description of the stories.
* List the first 3 series (if any) this hero takes part in with name and description of the series.
BONUS:
* Include a custom transition from the Listing view to the Detail view.

This user story and its bonus task was completed using the same libraries used in the previous story plus one external library in order to help to reduce the time of implementation: the ExpandableView library.

The UI of this user story is present in the CharacterDetailView, this view is also open without destroying the MainView in order to let the user keep the state of the MainActivity.

![DetailView](https://bitbucket.org/JRNFaria/marvelwiki/raw/23ed2a55c7dea8825de582002987de49ab43c3a7/screenshots/detailView.jpg)


Note: All the requests to the service were sorted by date, but the results may not come sorted, since the marvel service puts the default date at -0001-11-30T00:00:00-0500 (a negative date) when the publication date of the stories, series, events and comics are unknown.

### US-3:​ Search Heroes
ACCEPTANCE CRITERIA:
* The search query should be done by name.

This task took similar approaches to the first two, the search was available on top of the MainView making a new request to the marvel api service every time the user changed the string query, this choice improve the experience to the user but can have as consequence the creation of an excessive number rest request to the service.


![SearchView](https://bitbucket.org/JRNFaria/marvelwiki/raw/23ed2a55c7dea8825de582002987de49ab43c3a7/screenshots/mainViewSearch.jpg)

### US-4: ​Favourite Hero

* An hero can be marked or unmarked as a favourite both in the Listing and Detail views.
* All favourites should be persisted between app sessions.

BONUS:
* All favourites are listed in a specific Listing view.

In this user-story the room library was added to the project, the favourite star shown in the list of characters and detail of character views are ImageButtons that change their as the content of the database is being changed. On favourite insertion in the database the id, name and image path of the character is saved, this allows the user to see his favourites list even when there is no internet.

![SearchView](https://bitbucket.org/JRNFaria/marvelwiki/raw/23ed2a55c7dea8825de582002987de49ab43c3a7/screenshots/mainViewFavourites.jpg)

The bonus task of this user-story is located in the FavouritesListFragment in the MainView, and reutilizes the same layouts has characters list, since the information returned by the marvel service api for the pagination of the character list was transformed in order to be the same as the pagination returned by the room pagination

### Extra Features
Some small extra features were created:
* sort by name implementation (on the left of the search field), this button changes the name sort order from ascendant to descendant and vise-versa
* both sort and search operations create in user story 3 also work in the favourite list fragment too at the same time


## Future work

Some work was not finalized due of the time constraints like:

* More unit and UI tests (only the room database was tested)
* Improve small things in the interface like the press feedback of the retry button, the order that the expandable views appear in the Character detail layout and the possible removal of some layout that may not be contributing to the betterment of the project
* Allow the user to rotate screen (at the moment the app only works in portrait mode)
* replace ExpandableView library for other similar library that being supported by a more trustworthy user or create it myself to avoid problems like the loading of the views in random order in the future
* get a better and safer way to save the marvel service keys
* user story bonus task should only appear only the user reaches the end of the list in the layout
* some times the search and sort action are slow, some trick like extra loading some characters may help to improve the user experience
